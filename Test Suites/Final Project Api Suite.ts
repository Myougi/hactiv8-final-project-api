<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Final Project Api Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>9f3fafbb-109f-4481-93a8-ecab36225af0</testSuiteGuid>
   <testCaseLink>
      <guid>56b12ce8-984c-4ae0-8dcc-4b4d9e298f71</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Api Testing - Create User</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;Kamisato Ayaka&quot;</value>
         <variableId>da5259e9-deac-4e3c-a9fe-b4a2842e755e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;QA Engineer&quot;</value>
         <variableId>8772e9ed-9da1-40d2-a4b4-d36b39473e5c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>30f5580d-951d-49f6-aafb-d95277b48924</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Api Testing - Check User ID</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;2&quot;</value>
         <variableId>ac01ba68-9ab6-4d04-a1e6-82a6fc35efb2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5f689b6b-d349-44fc-8324-837bdc98da16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Api Testing - Update User using PUT</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;3&quot;</value>
         <variableId>91d5d692-4af8-40b1-ad53-97ea905b7194</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;Itaragaki Itto&quot;</value>
         <variableId>8e2766f0-fd0f-4211-906a-93fe36fe2fd3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;QA Engineer&quot;</value>
         <variableId>d7c19e3a-1faf-4ac5-9ef9-8da5df4c76bc</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9d1c2ade-bbf4-4deb-b2c1-2e30388b06be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Api Testing - Update User using PATCH</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;5&quot;</value>
         <variableId>5a6dd341-4451-4dec-bab0-3c507bacd881</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>SCRIPT_VARIABLE</type>
         <value>&quot;QA Engineer&quot;</value>
         <variableId>4228d469-152f-459a-af92-1ba680f4b86f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0d84c55d-1804-4d35-b198-261fbec0b19a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Api Testing - Delete User</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4baec74f-8ed4-4670-afb4-d912ed1ab098</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
