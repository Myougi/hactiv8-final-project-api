# Hactiv8 Final Project api

Repo : https://gitlab.com/Myougi/hactiv8-final-project-api.git

Scenario :
1. Membuat User Baru dengan Nama = Kamisato Ayaka dan Job = QA Engineer
2. Check User ID = 2 dan Verifikasi Nama Depan = Janet dan Nama Belakang = Weaver
3. Update name and job where user id = 3
4. Update job where user id = 5
5. Delete user yang telah dibuat dari endpoint Create User dan verifikasi response code = 204